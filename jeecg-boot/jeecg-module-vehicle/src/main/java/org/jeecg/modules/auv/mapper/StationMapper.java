package org.jeecg.modules.auv.mapper;

import org.jeecg.modules.auv.entity.Station;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: auv_station
 * @Author: jeecg-boot
 * @Date:   2024-08-26
 * @Version: V1.0
 */
public interface StationMapper extends BaseMapper<Station> {

}
