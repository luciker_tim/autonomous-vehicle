package org.jeecg.modules.auv.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.jeecg.modules.auv.config.JiushiConfig;
import org.jeecg.modules.auv.service.RemoteZelostechService;
import org.jeecg.modules.auv.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper; // 导入 Jackson ObjectMapper
/**
 * 九识远程控制接口
 * @author haoRt
 * @date 2024/8/27 20:07
 */
@Slf4j
@Service
public class RemoteZelostechServiceImpl implements RemoteZelostechService {

    private static final String STATIONS_API_URL = "/vehicle/sound_and_show";

    @Autowired
    private JiushiConfig jiushiConfig;

    private static final ObjectMapper objectMapper = new ObjectMapper(); // 创建 ObjectMapper 实例
    @Override
    public boolean playSound(SoundRequest soundRequest) {
        RestTemplate restTemplate = new RestTemplate();
        // 将参数对象转换为 JSON 字符串
        String jsonRequest;
        try {
            jsonRequest = objectMapper.writeValueAsString(soundRequest);
        } catch (Exception e) {
            throw new RuntimeException("Failed to convert request object to JSON", e);

        }


        // 设置 headers

        HttpHeaders headers = new HttpHeaders();

        headers.setContentType(MediaType.APPLICATION_JSON);

        headers.add("token", jiushiConfig.getJiushiToken()); // 示例：添加认证令牌

        // 创建 HttpEntity 对象

        HttpEntity<String> entity = new HttpEntity<>(jsonRequest, headers);


        // 发起 POST 请求并返回 JSON 字符串

        ResponseEntity<Result<String>> response = restTemplate.exchange(

                JiushiConfig.JIUSHI_OPENAPI_URL +  STATIONS_API_URL,

                HttpMethod.POST,

                entity,

                new ParameterizedTypeReference<Result<String>>() {}

        );


        if(response.getBody().isSuccess()){
            return true;
        }
        return false;
    }

}

