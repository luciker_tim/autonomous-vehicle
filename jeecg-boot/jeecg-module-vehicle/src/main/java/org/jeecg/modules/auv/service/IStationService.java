package org.jeecg.modules.auv.service;

import org.jeecg.modules.auv.entity.Station;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: auv_station
 * @Author: jeecg-boot
 * @Date:   2024-08-26
 * @Version: V1.0
 */
public interface IStationService extends IService<Station> {

}
