package org.jeecg.modules.auv.vo;

import lombok.Data;

@Data // 使用 Lombok 的 Data 注解自动生成 getter、setter 和构造方法等
public class SoundRequest {
    private String vehicleName;
    private String sound;
    private String show;
    private int showDuration;
}