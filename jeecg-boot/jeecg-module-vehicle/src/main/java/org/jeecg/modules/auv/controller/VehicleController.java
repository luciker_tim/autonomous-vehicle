package org.jeecg.modules.auv.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.auv.entity.Vehicle;
import org.jeecg.modules.auv.service.IVehicleService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.apache.shiro.authz.annotation.RequiresPermissions;

 /**
 * @Description: 巡逻车辆
 * @Author: jeecg-boot
 * @Date:   2024-08-26
 * @Version: V1.0
 */
@Api(tags="巡逻车辆")
@RestController
@RequestMapping("/auv/vehicle")
@Slf4j
public class VehicleController extends JeecgController<Vehicle, IVehicleService> {
	@Autowired
	private IVehicleService vehicleService;
	
	/**
	 * 分页列表查询
	 *
	 * @param vehicle
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "巡逻车辆-分页列表查询")
	@ApiOperation(value="巡逻车辆-分页列表查询", notes="巡逻车辆-分页列表查询")
	@GetMapping(value = "/list")
	public Result<IPage<Vehicle>> queryPageList(Vehicle vehicle,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<Vehicle> queryWrapper = QueryGenerator.initQueryWrapper(vehicle, req.getParameterMap());
		Page<Vehicle> page = new Page<Vehicle>(pageNo, pageSize);
		IPage<Vehicle> pageList = vehicleService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param vehicle
	 * @return
	 */
	@AutoLog(value = "巡逻车辆-添加")
	@ApiOperation(value="巡逻车辆-添加", notes="巡逻车辆-添加")
	@RequiresPermissions("auv:auv_vehicle:add")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody Vehicle vehicle) {
		vehicleService.save(vehicle);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param vehicle
	 * @return
	 */
	@AutoLog(value = "巡逻车辆-编辑")
	@ApiOperation(value="巡逻车辆-编辑", notes="巡逻车辆-编辑")
	@RequiresPermissions("auv:auv_vehicle:edit")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody Vehicle vehicle) {
		vehicleService.updateById(vehicle);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "巡逻车辆-通过id删除")
	@ApiOperation(value="巡逻车辆-通过id删除", notes="巡逻车辆-通过id删除")
	@RequiresPermissions("auv:auv_vehicle:delete")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		vehicleService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "巡逻车辆-批量删除")
	@ApiOperation(value="巡逻车辆-批量删除", notes="巡逻车辆-批量删除")
	@RequiresPermissions("auv:auv_vehicle:deleteBatch")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.vehicleService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "巡逻车辆-通过id查询")
	@ApiOperation(value="巡逻车辆-通过id查询", notes="巡逻车辆-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<Vehicle> queryById(@RequestParam(name="id",required=true) String id) {
		Vehicle vehicle = vehicleService.getById(id);
		if(vehicle==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(vehicle);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param vehicle
    */
    @RequiresPermissions("auv:auv_vehicle:exportXls")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, Vehicle vehicle) {
        return super.exportXls(request, vehicle, Vehicle.class, "巡逻车辆");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequiresPermissions("auv:auv_vehicle:importExcel")
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, Vehicle.class);
    }

}
