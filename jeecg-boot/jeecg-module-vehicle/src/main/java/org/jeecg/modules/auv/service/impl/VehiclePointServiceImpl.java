package org.jeecg.modules.auv.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.modules.auv.entity.VehiclePoint;
import org.jeecg.modules.auv.mapper.VehiclePointMapper;
import org.jeecg.modules.auv.service.IVehiclePointService;
import org.springframework.stereotype.Service;

/**
 * @Description: 任务巡逻点
 * @Author: jeecg-boot
 * @Date:   2024-08-26
 * @Version: V1.0
 */
@Service
public class VehiclePointServiceImpl extends ServiceImpl<VehiclePointMapper, VehiclePoint> implements IVehiclePointService {

}
