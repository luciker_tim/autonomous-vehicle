package org.jeecg.modules.auv.quartz;

import org.springframework.stereotype.Component;

@Component
public class MyTask {

    int i = 0;
//    @Scheduled(cron = "*/5 * * * * ?") //  5秒 直接在方法上使用注解即可完成扫描
    public  void  run(){
        System.out.println("----执行扫描一次---" + i++);
    }
}
