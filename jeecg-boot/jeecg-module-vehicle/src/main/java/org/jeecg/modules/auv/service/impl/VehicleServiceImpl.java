package org.jeecg.modules.auv.service.impl;

import org.jeecg.modules.auv.entity.Vehicle;
import org.jeecg.modules.auv.mapper.VehicleMapper;
import org.jeecg.modules.auv.service.IVehicleService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 巡逻车辆
 * @Author: jeecg-boot
 * @Date:   2024-08-26
 * @Version: V1.0
 */
@Service
public class VehicleServiceImpl extends ServiceImpl<VehicleMapper, Vehicle> implements IVehicleService {

}
