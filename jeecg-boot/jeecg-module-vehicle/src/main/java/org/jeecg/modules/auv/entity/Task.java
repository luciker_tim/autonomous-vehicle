package org.jeecg.modules.auv.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 巡逻任务
 * @Author: jeecg-boot
 * @Date:   2024-08-26
 * @Version: V1.0
 */
@Data
@TableName("auv_task")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="auv_task对象", description="巡逻任务")
public class Task implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键id")
    private java.lang.String id;
	/**巡逻任务名称*/
	@Excel(name = "巡逻任务名称", width = 15)
    @ApiModelProperty(value = "巡逻任务名称")
    private java.lang.String name;
	/**所属巡逻站id*/
	@Excel(name = "所属巡逻站id", width = 15)
    @ApiModelProperty(value = "所属巡逻站id")
    private java.lang.String stationId;
	/**所属巡逻站编号*/
	@Excel(name = "所属巡逻站编号", width = 15)
    @ApiModelProperty(value = "所属巡逻站编号")
    private java.lang.String stationNum;
	/**所属巡逻站名称*/
	@Excel(name = "所属巡逻站名称", width = 15)
    @ApiModelProperty(value = "所属巡逻站名称")
    private java.lang.String stationName;
	/**任务类型*/
	@Excel(name = "任务类型", width = 15)
    @ApiModelProperty(value = "任务类型")
    private java.lang.String taskType;
	/**任务开始时间*/
	@Excel(name = "任务开始时间", width = 15)
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "任务开始时间yyyy-MM-dd HH:mm:ss")
    private java.lang.String startTime;
	/**任务结束时间*/
	@Excel(name = "任务结束时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "任务结束时间yyyy-MM-dd HH:mm:ss")
    private java.util.Date endTime;
	/**任务状态*/
	@Excel(name = "任务状态", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "任务状态")
    private java.util.Date taskStatus;
	/**车辆id*/
	@Excel(name = "车辆id", width = 15)
    @ApiModelProperty(value = "车辆id")
    private java.lang.String vehicleId;
	/**车辆编号*/
	@Excel(name = "车辆编号", width = 15)
    @ApiModelProperty(value = "车辆编号")
    private java.lang.String vehicleNum;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private java.lang.String remarks;
	/**所属部门*/
	@Excel(name = "所属部门", width = 15)
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;

    @ApiModelProperty(value = "停靠点列表")
    @TableField(exist = false)
    private List<TaskPoint> taskPointList;
}
