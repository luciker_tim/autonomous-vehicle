package org.jeecg.modules.auv.quartz.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.auv.quartz.entity.AuvQuartzJob;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 定时任务在线管理
 * @Author: jeecg-boot
 * @Date:  2019-01-02
 * @Version: V1.0
 */
public interface AuvQuartzJobMapper extends BaseMapper<AuvQuartzJob> {

    /**
     * 根据jobClassName查询
     * @param jobClassName 任务类名
     * @return
     */
	public List<AuvQuartzJob> findByJobClassName(@Param("jobClassName") String jobClassName);

}
