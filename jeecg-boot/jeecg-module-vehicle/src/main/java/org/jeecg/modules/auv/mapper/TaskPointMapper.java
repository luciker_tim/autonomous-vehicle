package org.jeecg.modules.auv.mapper;

import org.jeecg.modules.auv.entity.TaskPoint;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 任务巡逻点
 * @Author: jeecg-boot
 * @Date:   2024-08-26
 * @Version: V1.0
 */
public interface TaskPointMapper extends BaseMapper<TaskPoint> {

}
