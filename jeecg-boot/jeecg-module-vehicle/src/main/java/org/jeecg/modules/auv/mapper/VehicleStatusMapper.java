package org.jeecg.modules.auv.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.auv.entity.Vehicle;

/**
 * @Description: 巡逻车辆
 * @Author: jeecg-boot
 * @Date:   2024-08-26
 * @Version: V1.0
 */
public interface VehicleStatusMapper extends BaseMapper<Vehicle> {

}
