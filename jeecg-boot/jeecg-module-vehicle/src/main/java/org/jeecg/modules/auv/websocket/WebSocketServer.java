package org.jeecg.modules.auv.websocket;

import cn.hutool.json.JSON;
import cn.hutool.json.JSONUtil;
import org.apache.commons.lang3.ObjectUtils;
import org.jeecg.modules.auv.websocket.eneity.StatusSocketMsg;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Component
@ServerEndpoint("/ws/auv/vehicle_status/{userId}")
public class WebSocketServer {

    private String userId;
    private Session session;

    /**
     * 固定前缀
     */
    private static final String USER_NAME_PREFIX = "user_name_";

    /**
     * 存放Session集合，方便推送消息 （jakarta.websocket）
     */
    public static ConcurrentHashMap<String, Session> sessionMap = new ConcurrentHashMap<>();

    /**
     * 存放房间用户集合，方便推送消息 （jakarta.websocket）
     */
    public static HashMap<String, List<String>> groupSessionMap = new HashMap<>();

    public static HashMap<String, String> vehicleSessionMap = new HashMap<>();

    /**
     * 监听：连接成功
     *
     * @param session
     * @param userId 连接的用户名
     */
    @OnOpen
    public void onOpen(Session session, @PathParam("userId") String userId) {
//        this.userName = userName;
//        this.session = session;
//        String name = USER_NAME_PREFIX + userName;
        if (!sessionMap.containsKey(userId)) {
            sessionMap.put(userId, session);
            this.userId = userId;
            // 在线数加1
//            String tips = userName + " 加入聊天室。当前聊天室人数为" + sessionMap.size();
//            System.out.println(tips);
//            publicMessage(tips);
        }
        System.out.println(sessionMap.size());
    }

    /**
     * 监听: 连接关闭
     */
    @OnClose
    public void onClose() {
        // 连接关闭后，将此websocket从set中删除
        sessionMap.remove(userId);
        if (vehicleSessionMap.containsKey(userId))
        {
            vehicleSessionMap.remove(userId);
        }

//        String tips = userName + " 退出聊天室。当前聊天室人数为" + sessionMap.size();
//        System.out.println(tips);
//        publicMessage(tips);
    }

    /**
     * 监听：收到客户端发送的消息
     *
     * @param message 发送的信息（json格式，里面是 SocketMsg 的信息）
     */
    @OnMessage
    public void onMessage(String message) {
        if (JSONUtil.isTypeJSONObject(message)) {
            StatusSocketMsg socketMsg = JSONUtil.toBean(message, StatusSocketMsg.class);
            System.out.println(socketMsg.toString());
            vehicleSessionMap.put(socketMsg.getUserId(), socketMsg.getVehicleName());
//            List<String> vehicles = vehicleSessionMap.get("userId");
//            if (vehicles.contains(socketMsg.getVehicleName()))
//            groupMessage(socketMsg);
//            if (socketMsg.getType() == 2) {
//                // 群聊，需要找到发送者和房间ID
//                groupMessage(socketMsg);
//            } else if (socketMsg.getType() == 1) {
//                // 单聊，需要找到发送者和接受者
//                privateMessage(socketMsg);
//            } else {
//                // 全局广播群发消息
//                publicMessage(socketMsg.getMsg());
//            }
        }
    }

    /**
     * 监听：发生异常
     *
     * @param error
     */
    @OnError
    public void onError(Throwable error) {
        System.out.println("userName为：" + userId + "，发生错误：" + error.getMessage());
        error.printStackTrace();
    }

    /**
     * 群发
     *
     * @param message
     */
    public void sendToAllClient(String message) {
        //将前端传过来的数据群发, 所有人都能拿到会话   values拿到map集合的所有属性值
        Collection<Session> sessions = sessionMap.values();
        for (Session session : sessions) {
            try {
                //服务器向客户端发送消息
                session.getBasicRemote().sendText(message);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 群聊：向指定房间ID推送消息
     */
    public synchronized static void groupMessage(StatusSocketMsg socketMsg) {
        // 存储房间号和用户信息
        String vehicleName = socketMsg.getVehicleName();
        // 判断是否有这个房间
        List<String> strings = groupSessionMap.get(vehicleName);
        if (ObjectUtils.isEmpty(strings)) {
            List<String> users = new ArrayList<>();
            users.add(socketMsg.getUserId());
            groupSessionMap.put(vehicleName, users);
        } else {
            // 这里应该写接口，先添加房间ID，简易写法直接传过来
            List<String> users = groupSessionMap.get(vehicleName);
//            String sendOutUser = socketMsg.getSendOutUser();
            boolean contains = users.contains(socketMsg.getUserId());
            if (!contains) {
                users.add(socketMsg.getUserId());
            }
        }

        // 发送给接收者
        if (vehicleName != null) {
            // 发送给接收者
//            System.out.println(socketMsg.getSendOutUser() + " 向房间 【 " + roomId + " 】 发送了一条消息：" + socketMsg.getMsg());
            // 此时要判断房间有哪些人，把这些消息定向发给处于此房间的用户
            List<String> roomUser = groupSessionMap.get(vehicleName);
            for (String userId : roomUser) {
                System.out.println(sessionMap.size());
                // 接收消息的用户
                System.out.println(userId);
                Session receiveUser = sessionMap.get(userId);
//                receiveUser.getAsyncRemote().sendText(socketMsg.getSendOutUser() + " 向房间 【 " + roomId + " 】 发送了一条消息：" + socketMsg.getMsg());
                String jsonStr = "";
                JSON json;
                if (socketMsg.getVehicleData() != null)
                {
                    json = JSONUtil.parse(socketMsg.getVehicleData());
                    jsonStr = json.toString();
                }

                receiveUser.getAsyncRemote().sendText(jsonStr);
            }
        } else {
            // 发送消息的用户
//            System.out.println(socketMsg.getSendOutUser() + " 私聊的用户 " + socketMsg.getReceiveUser() + " 不在线或者输入的用户名不对");
//            Session sendOutUser = sessionMap.get(USER_NAME_PREFIX + socketMsg.getSendOutUser());
            // 将系统提示推送给发送者
//            sendOutUser.getAsyncRemote().sendText("系统消息：对方不在线或者您输入的用户名不对");
        }
    }

    /**
     * 私聊：向指定客户端推送消息
     */
    public synchronized static void privateMessage(StatusSocketMsg socketMsg) {
        // 接收消息的用户
        Session receiveUser = sessionMap.get(USER_NAME_PREFIX + socketMsg.getReceiveUser());
        // 发送给接收者
        if (receiveUser != null) {
            // 发送给接收者
            System.out.println(socketMsg.getSendOutUser() + " 向 " + socketMsg.getReceiveUser() + " 发送了一条消息：" + socketMsg.getMsg());
            receiveUser.getAsyncRemote().sendText(socketMsg.getSendOutUser() + "：" + socketMsg.getMsg());
        } else {
            // 发送消息的用户
            System.out.println(socketMsg.getSendOutUser() + " 私聊的用户 " + socketMsg.getReceiveUser() + " 不在线或者输入的用户名不对");
            Session sendOutUser = sessionMap.get(USER_NAME_PREFIX + socketMsg.getSendOutUser());
            // 将系统提示推送给发送者
            sendOutUser.getAsyncRemote().sendText("系统消息：对方不在线或者您输入的用户名不对");
        }
    }

    /**
     * 私聊：向指定客户端推送消息
     */
    public synchronized static void sendStatus(StatusSocketMsg socketMsg) {
        // 接收消息的用户
        Session receiveUser = sessionMap.get(USER_NAME_PREFIX + socketMsg.getReceiveUser());
        // 发送给接收者
        if (receiveUser != null) {
            // 发送给接收者
            System.out.println(socketMsg.getSendOutUser() + " 向 " + socketMsg.getReceiveUser() + " 发送了一条消息：" + socketMsg.getMsg());
            receiveUser.getAsyncRemote().sendText(socketMsg.getSendOutUser() + "：" + socketMsg.getMsg());
        } else {
            // 发送消息的用户
            System.out.println(socketMsg.getSendOutUser() + " 私聊的用户 " + socketMsg.getReceiveUser() + " 不在线或者输入的用户名不对");
            Session sendOutUser = sessionMap.get(USER_NAME_PREFIX + socketMsg.getSendOutUser());
            // 将系统提示推送给发送者
            sendOutUser.getAsyncRemote().sendText("系统消息：对方不在线或者您输入的用户名不对");
        }
    }

    /**
     * 全局消息：公开聊天记录
     *
     * @param message 发送的消息
     */
    public synchronized static void publicMessage(String message) {
        for (String username : sessionMap.keySet()) {
            Session session = sessionMap.get(username);
            session.getAsyncRemote().sendText("公共频道接收了一条消息：" + message);
            System.out.println("公共频道接收了一条消息：" + message);
        }
    }
}
