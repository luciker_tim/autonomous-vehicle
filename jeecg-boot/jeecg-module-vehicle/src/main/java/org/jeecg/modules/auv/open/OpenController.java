package org.jeecg.modules.auv.open;

import org.jeecg.modules.auv.config.JiushiConfig;
import org.jeecg.common.api.vo.Result;

import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.auv.service.RemoteZelostechService;
import org.jeecg.modules.auv.service.VehicleZelostechService;
import org.jeecg.modules.auv.util.MysqlUtil;
import org.jeecg.modules.auv.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @Description: 开放API
 * @Author: jeecg-boot
 * @Date:   2024-08-26
 * @Version: V1.0
 */
@Api(tags="开放API")
@RestController
@RequestMapping("/open")
@Slf4j
public class OpenController {

	@Autowired
	private JiushiConfig jiushiConfig;
	@Autowired
	private MysqlUtil mysqlUtil;

	/**
	* @return
	*/
	@ApiOperation(value="开放api")
	@GetMapping(value = "/test")
	public Result<String> test() {
		System.out.println("测试");
		String jiushiToken = jiushiConfig.getJiushiToken();
		return Result.OK(jiushiToken);
	}


	/**
	 * @return
	 */
	@ApiOperation(value="开放api2")
	@GetMapping(value = "/testUtil")
	public Result<String> testUtil(@RequestParam(name="name") String name) {
		System.out.println("测试");
		String nextValue = ""+mysqlUtil.getNextSequenceValue(name);
		return Result.OK(nextValue);
	}
}
