package org.jeecg.modules.auv.vo;
import lombok.Data;
import java.util.List;


@Data

public class JsListPageData<T> {

    private Integer total;

    private List<T> list;

    private Integer pageNum;

    private Integer pageSize;

    private Integer size;

    private Integer startRow;

    private Integer endRow;

    private Integer pages;

    private Integer prePage;

    private Integer nextPage;

    private boolean isFirstPage;

    private boolean isLastPage;

    private boolean hasPreviousPage;

    private boolean hasNextPage;

    private Integer navigatePages;

    private List<Integer> navigatepageNums;

    private Integer navigateFirstPage;

    private Integer navigateLastPage;

}