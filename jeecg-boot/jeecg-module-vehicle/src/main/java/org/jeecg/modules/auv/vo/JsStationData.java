package org.jeecg.modules.auv.vo;

import lombok.Data;


@Data
public class JsStationData {
    private int id;
    private String name;
    private String lon;
    private String lat;
    private String gcj02Lon;
    private String gcj02Lat;

    private String latAndLon;
    private String gcj02LatAndLon;
}