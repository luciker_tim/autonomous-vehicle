package org.jeecg.modules.auv.config;

import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Slf4j
@Component
public class JiushiConfig {

    public static String JIUSHI_OPENAPI_URL = "http://gateway.zelostech.com.cn/business-server/open-apis";
    public static String JIUSHI_APP_URL = "https://auth.zelostech.com.cn/app";

    @Autowired
    private RedisUtil redisUtil;
    private String jiushiTokenConstant = "jiushi:token";

    public String getJiushiToken(){

        if (redisUtil.getExpire(jiushiTokenConstant) > 30){
            return (String) redisUtil.get(jiushiTokenConstant);
        }

        String url = JIUSHI_APP_URL + "/accessToken";
        HashMap<String, Object> paramMap = new HashMap<>();
        paramMap.put("appId","csnlrih5if15ofg5sswu24otxn5husge");
        paramMap.put("appKey","RnDSZ5dd9s3bY7V2BXm39uLkBwQJH5D0ZcijSeOwytL56R8TKZI5jXBnPj446vEw");
        String requestJson = JSONUtil.toJsonStr(paramMap);
        String res = HttpRequest.post(url).body(requestJson,"application/json").execute().body();

        JSONObject resJsonObject = JSONUtil.parseObj(res);
        String data = resJsonObject.getStr("data");
        JSONObject dataJSONObject = JSONUtil.parseObj(data);
        String token = dataJSONObject.getStr("token");
        redisUtil.set("jiushi:token", token, 1440);

        return token;
    }
}
