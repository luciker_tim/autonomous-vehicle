package org.jeecg.modules.auv.service;

import org.jeecg.modules.auv.vo.*;

/**
 * 九识远程控制接口
 * @author haoRt
 * @date 2024/8/26 20:08
 */
public interface RemoteZelostechService {

    boolean playSound(SoundRequest soundRequest);

}
