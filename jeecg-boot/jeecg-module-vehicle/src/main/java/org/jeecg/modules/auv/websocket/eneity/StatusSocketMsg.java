package org.jeecg.modules.auv.websocket.eneity;

import lombok.Data;
import org.jeecg.modules.auv.vo.JsVehicleData;

@Data
public class StatusSocketMsg {

    /**
     * 聊天类型 0 全局广播 1 单聊 2 群聊
     **/
    private int type;
    /**
     * 发送者
     **/
    private String sendOutUser;
    /**
     * 接受者
     **/
    private String receiveUser;
    /**
     * 房间id
     **/
    private String roomId;
    /**
     * 消息
     **/
    private String msg;

    private String userId;

    private JsVehicleData vehicleData;

    private String vehicleName;
}
