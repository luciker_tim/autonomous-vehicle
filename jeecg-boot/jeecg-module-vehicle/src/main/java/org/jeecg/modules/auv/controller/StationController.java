package org.jeecg.modules.auv.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.auv.entity.Station;
import org.jeecg.modules.auv.service.IStationService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.apache.shiro.authz.annotation.RequiresPermissions;

 /**
 * @Description: auv_station
 * @Author: jeecg-boot
 * @Date:   2024-08-26
 * @Version: V1.0
 */
@Api(tags="巡逻站")
@RestController
@RequestMapping("/auv/station")
@Slf4j
public class StationController extends JeecgController<Station, IStationService> {
	@Autowired
	private IStationService stationService;
	
	/**
	 * 分页列表查询
	 *
	 * @param station
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "auv_station-分页列表查询")
	@ApiOperation(value="auv_station-分页列表查询", notes="auv_station-分页列表查询")
	@GetMapping(value = "/list")
	public Result<IPage<Station>> queryPageList(Station station,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<Station> queryWrapper = QueryGenerator.initQueryWrapper(station, req.getParameterMap());
		Page<Station> page = new Page<Station>(pageNo, pageSize);
		IPage<Station> pageList = stationService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param station
	 * @return
	 */
	@AutoLog(value = "auv_station-添加")
	@ApiOperation(value="auv_station-添加", notes="auv_station-添加")
	@RequiresPermissions("auv:auv_station:add")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody Station station) {
		stationService.save(station);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param station
	 * @return
	 */
	@AutoLog(value = "auv_station-编辑")
	@ApiOperation(value="auv_station-编辑", notes="auv_station-编辑")
	//@RequiresPermissions("auv:auv_station:edit")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody Station station) {
		stationService.updateById(station);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "auv_station-通过id删除")
	@ApiOperation(value="auv_station-通过id删除", notes="auv_station-通过id删除")
	@RequiresPermissions("auv:auv_station:delete")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		stationService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "auv_station-批量删除")
	@ApiOperation(value="auv_station-批量删除", notes="auv_station-批量删除")
	@RequiresPermissions("auv:auv_station:deleteBatch")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.stationService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "auv_station-通过id查询")
	@ApiOperation(value="auv_station-通过id查询", notes="auv_station-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<Station> queryById(@RequestParam(name="id",required=true) String id) {
		Station station = stationService.getById(id);
		if(station==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(station);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param station
    */
    @RequiresPermissions("auv:auv_station:exportXls")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, Station station) {
        return super.exportXls(request, station, Station.class, "auv_station");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequiresPermissions("auv:auv_station:importExcel")
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, Station.class);
    }

}
