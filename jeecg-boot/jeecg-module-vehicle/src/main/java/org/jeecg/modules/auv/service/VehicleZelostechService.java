package org.jeecg.modules.auv.service;

import org.jeecg.modules.auv.entity.VehiclePoint;
import org.jeecg.modules.auv.vo.JsStationData;
import org.jeecg.modules.auv.vo.JsStopData;
import org.jeecg.modules.auv.vo.JsVehicleData;
import org.jeecg.modules.auv.vo.JsListPageData;

import java.util.ArrayList;
import java.util.List;

/**
 * 九识信息查询接口
 * @author haoRt
 * @date 2024/1/26 20:08
 */
public interface VehicleZelostechService {
    /**
     * 查询聊天记录
     * @return
     * @author chenrui
     * @date 2024/2/22 13:59
     */
    JsVehicleData getByVehicleName(String vehicleName);
    JsListPageData<JsVehicleData> getVehicleList(String pageNumber, String pageSize, String stationId);
    JsListPageData<JsStopData> getStopList(String pageNumber, String pageSize, String stationId);
    JsListPageData<JsStationData> getStationList(String pageNumber, String pageSize);

    ArrayList<VehiclePoint> getStopsByVehicleName (String vehicleName);

}
