package org.jeecg.modules.auv.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.modules.auv.entity.Point;
import org.jeecg.modules.auv.mapper.PointMapper;
import org.jeecg.modules.auv.service.IPointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import javax.annotation.Resource;

/**
 * @Description: 巡逻点
 * @Author: jeecg-boot
 * @Date:   2024-08-26
 * @Version: V1.0
 */
@Service
public class PointServiceImpl extends ServiceImpl<PointMapper, Point> implements IPointService {
    @Resource
    PointMapper pointMapper;

    public boolean changeActive(Point point) {
        return pointMapper.changeActive(point);
    }

    public boolean changeType(Point point){
        return pointMapper.changeType(point);
    }

    public IPage<Point> pageList (Page<Point> page, Wrapper<Point> queryWrapper) {
        return null;
    }
}
