package org.jeecg.modules.auv.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.auv.entity.Point;
import org.jeecg.modules.auv.service.IPointService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.apache.shiro.authz.annotation.RequiresPermissions;

 /**
 * @Description: 巡逻点
 * @Author: jeecg-boot
 * @Date:   2024-08-26
 * @Version: V1.0
 */
@Api(tags="巡逻点")
@RestController
@RequestMapping("/auv/point")
@Slf4j
public class PointController extends JeecgController<Point, IPointService> {
	@Autowired
	private IPointService pointService;
	
	/**
	 * 分页列表查询
	 *
	 * @param point
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "巡逻点-分页列表查询")
	@ApiOperation(value="巡逻点-分页列表查询", notes="巡逻点-分页列表查询")
	@GetMapping(value = "/list")
	public Result<IPage<Point>> queryPageList(Point point,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<Point> queryWrapper = QueryGenerator.initQueryWrapper(point, req.getParameterMap());
		Page<Point> page = new Page<Point>(pageNo, pageSize);
		IPage<Point> pageList = pointService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param point
	 * @return
	 */
	@AutoLog(value = "巡逻点-添加")
	@ApiOperation(value="巡逻点-添加", notes="巡逻点-添加")
	//@RequiresPermissions("auv:auv_point:add")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody Point point) {
		pointService.save(point);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param point
	 * @return
	 */
	@AutoLog(value = "巡逻点-编辑")
	@ApiOperation(value="巡逻点-编辑", notes="巡逻点-编辑")
	//@RequiresPermissions("auv:auv_point:edit")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody Point point) {
		pointService.updateById(point);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "巡逻点-通过id删除")
	@ApiOperation(value="巡逻点-通过id删除", notes="巡逻点-通过id删除")
	@RequiresPermissions("auv:auv_point:delete")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		pointService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "巡逻点-批量删除")
	@ApiOperation(value="巡逻点-批量删除", notes="巡逻点-批量删除")
	@RequiresPermissions("auv:auv_point:deleteBatch")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.pointService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@ApiOperation(value="巡逻点-通过id查询", notes="巡逻点-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<Point> queryById(@RequestParam(name="id",required=true) String id) {
		Point point = pointService.getById(id);
		if(point==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(point);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param point
    */
    @RequiresPermissions("auv:auv_point:exportXls")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, Point point) {
        return super.exportXls(request, point, Point.class, "巡逻点");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequiresPermissions("auv:auv_point:importExcel")
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, Point.class);
    }


	 /**
	  *  修改可用状态
	  *
	  * @param point
	  * @return
	  */
	 @AutoLog(value = "巡逻点-修改可用状态")
	 @ApiOperation(value="巡逻点-修改可用状态", notes="巡逻点-修改可用状态")
	 @RequestMapping(value = "/changeActive", method = {RequestMethod.PUT,RequestMethod.POST})
	 public Result<String> changeActive(@RequestBody Point point) {
		 pointService.changeActive(point);
		 return Result.OK("修改可用状态成功!");
	 }
	 /**
	  *  修改类型
	  *
	  * @param point
	  * @return
	  */
	 @AutoLog(value = "巡逻点-修改停靠点类型")
	 @ApiOperation(value="巡逻点-修改停靠点类型", notes="巡逻点-修改停靠点类型")
	 @RequestMapping(value = "/changeType", method = {RequestMethod.PUT,RequestMethod.POST})
	 public Result<String> changeType(@RequestBody Point point) {
		 pointService.changeType(point);
		 return Result.OK("修改可用状态成功!");
	 }


	 //@AutoLog(value = "巡逻点-分页列表查询")
	 @ApiOperation(value="巡逻点-分页列表查询", notes="巡逻点-web分页列表查询")
	 @GetMapping(value = "/pageList")
	 public Result<IPage<Point>> queryWebPageList(Point point,
											   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
											   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
											   HttpServletRequest req) {
		 QueryWrapper<Point> queryWrapper = QueryGenerator.initQueryWrapper(point, req.getParameterMap());
		 Page<Point> page = new Page<Point>(pageNo, pageSize);
		 IPage<Point> pageList = pointService.pageList(page, queryWrapper);
		 return Result.OK(pageList);
	 }
}
