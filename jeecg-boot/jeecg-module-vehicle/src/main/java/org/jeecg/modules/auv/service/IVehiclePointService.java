package org.jeecg.modules.auv.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.auv.entity.VehiclePoint;

/**
 * @Description: 任务巡逻点
 * @Author: jeecg-boot
 * @Date:   2024-08-26
 * @Version: V1.0
 */
public interface IVehiclePointService extends IService<VehiclePoint> {

}
