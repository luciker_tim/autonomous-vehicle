package org.jeecg.modules.auv.service;

import org.jeecg.modules.auv.entity.TaskPoint;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 任务巡逻点
 * @Author: jeecg-boot
 * @Date:   2024-08-26
 * @Version: V1.0
 */
public interface ITaskPointService extends IService<TaskPoint> {

}
