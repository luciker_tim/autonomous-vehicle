package org.jeecg.modules.auv.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import io.swagger.annotations.Api;
import org.jeecg.modules.auv.entity.Point;
import org.jeecg.modules.auv.entity.Station;
import org.jeecg.modules.auv.entity.Vehicle;
import org.jeecg.modules.auv.entity.VehiclePoint;
import org.jeecg.modules.auv.service.*;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.auv.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 九识信息查询接口
 * @author: haoRt
 */
@Api(tags="九识转发接口")
@Slf4j
@RestController
@RequestMapping("/auv/zelostech")
public class ZelostechController {

    @Resource
    private VehicleZelostechService vehicleZelostechService;
    @Autowired
    private RemoteZelostechService remoteZelostechService;
    @Resource
    private IVehicleService iVehicleService;
    @Resource
    private IPointService iPointService;
    @Resource
    private IStationService iStationService;
    @Resource
    private IVehiclePointService iVehiclePointService;

    private String JIANS_SYSORGCODE = "1831571172580167681";
    private String PATROL_ID  = "1831583210048151554";


    /**
     * 通过 九识车辆编码 查询 车辆状态
     *
     * @param js_vehicleName
     * @return
     */
    @ApiOperation(value="巡逻车辆详情-通过 js_vehicleName 查询", notes="巡逻车辆-通过js_vehicleName查询")
    @GetMapping(value = "/getByVehicleName")
    public Result<JsVehicleData> getByVehicleName(@RequestParam(name="js_vehicleName",required=true) String js_vehicleName) {
        JsVehicleData vehicle = vehicleZelostechService.getByVehicleName(js_vehicleName);
        if(vehicle==null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(vehicle);
    }
    /**
     * 查询车辆列表
     *
     * @param pageNumber
     * @param pageSize
     * @param stationId
     * @return
     */
    @ApiOperation(value="九识车辆列表查询", notes="九识车辆列表查询")
    @GetMapping(value = "/getVehicleList")
    public Result<JsListPageData<JsVehicleData>> getVehicleList(
            @RequestParam(name="pageNumber") String pageNumber,
            @RequestParam(name="pageSize") String pageSize,
            @RequestParam(name="stationId") String stationId
    ) {
        JsListPageData<JsVehicleData> vl = vehicleZelostechService.getVehicleList(pageNumber,pageSize,stationId);
        if(vl==null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(vl);
    }

    /**
     * 查询
     *
     * @param pageNumber
     * @param pageSize
     * @param stationId
     * @return
     */
    @ApiOperation(value="查询九识停靠点列表")
    @GetMapping(value = "/getStopList")
    public Result<JsListPageData<JsStopData>> getStopList(
            @RequestParam(name="pageNumber",required=false) String pageNumber,
            @RequestParam(name="pageSize",required=false) String pageSize,
            @RequestParam(name="stationId") String stationId
    ) {
        JsListPageData<JsStopData> stopList = vehicleZelostechService.getStopList(pageNumber,pageSize,stationId);
        if( stopList == null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(stopList);
    }

    @ApiOperation(value="查询九识站点列表")
    @GetMapping(value = "/getStationList")
    public Result<JsListPageData<JsStationData>> getStationList(
            @RequestParam(name="pageNumber",required=false) String pageNumber,
            @RequestParam(name="pageSize",required=false) String pageSize
    ) {
        JsListPageData<JsStationData> stationList = vehicleZelostechService.getStationList(pageNumber,pageSize);
        if( stationList == null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(stationList);
    }
    @ApiOperation(value="九识语音播报下发")
    @PostMapping(value = "/sound")
    public Result<JsListPageData<JsStationData>> zelostechSound(@RequestBody SoundRequest soundRequest){
        boolean rs = remoteZelostechService.playSound(soundRequest);
        if( rs != true ) {
            return Result.error("语音播报调用失败");
        }
        return Result.OK();
    }


    /**
     * 同步车辆列表
     *
     * @param pageNumber
     * @param pageSize
     * @param stationId
     * @return
     */
    @ApiOperation(value="同步车辆列表", notes="同步车辆列表")
    @GetMapping(value = "/etlVehicleList")
    public Result<String> etlVehicleList(
            @RequestParam(name="pageNumber") String pageNumber,
            @RequestParam(name="pageSize") String pageSize,
            @RequestParam(name="stationId") String stationId
    ) {
        //通过九识接口查询车辆列表
        JsListPageData<JsVehicleData> vl = vehicleZelostechService.getVehicleList(pageNumber,pageSize,stationId);
        if(vl==null) {
            return Result.error("执行失败，九识车辆列表为空！");
        }

        //遍历车辆列表
        for (JsVehicleData jsvd : vl.getList()) {
            //查询是否车辆已迁移过,已存在则跳过迁移，比对关键字段为车辆名(jsvd.name())
            QueryWrapper vq = new QueryWrapper<Vehicle>();
            vq.eq("js_name",jsvd.getName());
            Vehicle vj = iVehicleService.getOne(vq);
            if(vj != null){
                continue;
            }

            //映射字段
            Vehicle v = new Vehicle();
            v.setLicence(jsvd.getNumber());
            v.setStationId(""+jsvd.getStationId());
            v.setStationName(jsvd.getStationName());
            v.setStatus(jsvd.getBusinessStatus());
            v.setTaskStatus(jsvd.getBusinessStatusName());
            v.setJsId(jsvd.getId());
            v.setJsName(jsvd.getName());
            //需求未明确字段
            v.setNum("etl"+jsvd.getNumber());
            v.setName("etl"+jsvd.getName());
            //默认设置为戬神科技的id值
            v.setSysOrgCode(JIANS_SYSORGCODE);

            iVehicleService.save(v);
        }
        return Result.OK("Success!!etl执行成功，请核对数据库。");
    }
    /**
     * 同步停靠点列表
     *
     * @param pageNumber
     * @param pageSize
     * @param stationId
     * @return
     */
    @ApiOperation(value="同步停靠点列表", notes="同步停靠点列表")
    @GetMapping(value = "/etlStopList")
    public Result<JsListPageData<JsStopData>> etlStopList(
            @RequestParam(name="pageNumber",required=false) String pageNumber,
            @RequestParam(name="pageSize",required=false) String pageSize,
            @RequestParam(name="stationId") String stationId
    ){

        //通过九识接口查询停靠点列表
        JsListPageData<JsStopData> stopList = vehicleZelostechService.getStopList(pageNumber,pageSize,stationId);
        if( stopList == null) {
            return Result.error("未找到对应数据");
        }

        //遍历车辆列表
        for (JsStopData jssd : stopList.getList()) {

            //查询是否停靠点已迁移过,已存在则跳过迁移，比对关键字段为停靠点id(jssd.name)
            QueryWrapper pq = new QueryWrapper<Point>();
            pq.eq("js_id",jssd.getId());
            Point pj = iPointService.getOne(pq);
            if(pj != null){
                continue;
            }

            //映射字段
            Point p = new Point();
            p.setName(jssd.getName());
            p.setLongitude("" + jssd.getGcj02Lon());
            p.setLatitude("" + jssd.getGcj02Lat());
            p.setInterLongitude("" + jssd.getLon());
            p.setInterLatitude("" + jssd.getLat());
            p.setJsId(jssd.getId());
            p.setJsStationId(jssd.getStationId());
            p.setJsStationName(jssd.getStationName());

            //临时规则字段
            p.setNum("XLD"+IdWorker.getIdStr());
            //默认设置为巡逻点的id值
            p.setType(PATROL_ID);
            //默认设置为戬神科技的id值
            p.setSysOrgCode(JIANS_SYSORGCODE);

            iPointService.save(p);
        }
        return Result.OK("Success!!etl执行成功，请核对数据库。");
    }

    /**
     * 同步站点列表
     *
     * @param pageNumber
     * @param pageSize
     * @return
     */
    @ApiOperation(value="同步站点列表", notes="同步站点列表")
    @GetMapping(value = "/etlStationList")
    public Result<JsListPageData<JsStopData>> etlStationList(
            @RequestParam(name="pageNumber",required=false) String pageNumber,
            @RequestParam(name="pageSize",required=false) String pageSize
    ){

        //通过九识接口查询停靠点列表
        JsListPageData<JsStationData> sl = vehicleZelostechService.getStationList(pageNumber,pageSize);
        if( sl == null) {
            return Result.error("未找到对应数据");
        }

        //遍历车辆列表
        for (JsStationData jssd : sl.getList()) {

            //查询是否停靠点已迁移过,已存在则跳过迁移，比对关键字段为停靠点id(jssd.name)
            QueryWrapper sq = new QueryWrapper<Station>();
            sq.eq("js_id",jssd.getId());
            Station pj = iStationService.getOne(sq);
            if(pj != null){
                continue;
            }

            //映射字段
            Station s = new Station();
            s.setName(jssd.getName());
            s.setLongitude("" + jssd.getGcj02Lon());
            s.setLatitude("" + jssd.getGcj02Lat());
            s.setInterLongitude("" + jssd.getLon());
            s.setInterLatitude("" + jssd.getLat());
            s.setJsId(jssd.getId());

            //临时规则字段
            s.setNum(""+IdWorker.getIdStr());
            //默认设置为戬神科技的id值
            s.setSysOrgCode(JIANS_SYSORGCODE);

            iStationService.save(s);
        }
        return Result.OK("Success!!etl执行成功，请核对数据库。");
    }

    /**
     * 同步车辆-停靠点绑定列表
     * （注，未加条件查询）
     *
     * @param pageNumber
     * @param pageSize
     * @return
     */
    @ApiOperation(value="同步车辆停靠点绑定列表", notes="同步车辆停靠点绑定列表")
    @GetMapping(value = "/etlVStopList")
    public Result<JsListPageData<JsStopData>> etlVStopList(
            @RequestParam(name="pageNumber",required=false) String pageNumber,
            @RequestParam(name="pageSize",required=false) String pageSize
    ){

        //查询戬神平台车辆列表
        List<Vehicle> vl = iVehicleService.list();
        if( vl == null) {
            return Result.error("无车辆数据");
        }

        for (Vehicle v : vl ) {
            ArrayList<VehiclePoint> stopsByVehicleName = vehicleZelostechService.getStopsByVehicleName(v.getJsName());
            iVehiclePointService.saveBatch(stopsByVehicleName);
        }
        return Result.OK("Success!!etl执行成功，请核对数据库。");
    }


}
