package org.jeecg.modules.auv.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.auv.entity.VehiclePoint;

/**
 * @Description: 任务巡逻点
 * @Author: jeecg-boot
 * @Date:   2024-08-26
 * @Version: V1.0
 */
public interface VehiclePointMapper extends BaseMapper<VehiclePoint> {

}
