package org.jeecg.modules.auv.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

    /**
     * 车辆-停靠点关联表
     */
@Data
@TableName("auv_vehicle_point")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="VehiclePoint对象", description="车辆-停靠点关联表")
public class VehiclePoint implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 主键 ID */
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "主键 ID")
    private Integer id;

    /** 戬神车辆 ID */
    @ApiModelProperty(value = "戬神车辆 ID")
    private Integer vehicleId;

    /** 九识车辆 ID */
    @ApiModelProperty(value = "九识车辆 ID")
    private Integer jsVehicleId;

    /** 戬神停靠点 ID */
    @ApiModelProperty(value = "戬神停靠点 ID")
    private Integer pointId;

    /** 九识停靠点 ID */
    @ApiModelProperty(value = "九识停靠点 ID")
    private Integer jsStopId;

    /** 创建人 */
    @ApiModelProperty(value = "创建人")
    private String createdBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createdAt;

    /** 修改人 */
    @ApiModelProperty(value = "修改人")
    private String modifiedBy;

    /** 修改时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime modifiedAt;

    /** 逻辑删除标记（0：未删除，1：已删除） */
    @ApiModelProperty(value = "逻辑删除标记（0：未删除，1：已删除）")
    private Boolean isDeleted;
}
