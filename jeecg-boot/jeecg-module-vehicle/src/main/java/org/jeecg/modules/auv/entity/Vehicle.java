package org.jeecg.modules.auv.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 巡逻车辆
 * @Author: jeecg-boot
 * @Date:   2024-08-26
 * @Version: V1.0
 */
@Data
@TableName("auv_vehicle")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="auv_vehicle对象", description="巡逻车辆")
public class Vehicle implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键id")
    private java.lang.String id;
	/**车辆编码*/
	@Excel(name = "车辆编码", width = 15)
    @ApiModelProperty(value = "车辆编码")
    private java.lang.String num;
	/**车辆名称*/
	@Excel(name = "车辆名称", width = 15)
    @ApiModelProperty(value = "车辆名称")
    private java.lang.String name;
	/**车辆牌照*/
	@Excel(name = "车辆牌照", width = 15)
    @ApiModelProperty(value = "车辆牌照")
    private java.lang.String licence;
	/**巡逻站id*/
	@Excel(name = "巡逻站id", width = 15)
    @ApiModelProperty(value = "巡逻站id")
    private java.lang.String stationId;
	/**巡逻站名称*/
	@Excel(name = "巡逻站名称", width = 15)
    @ApiModelProperty(value = "巡逻站名称")
    private java.lang.String stationName;
	/**车辆状态*/
	@Excel(name = "车辆状态", width = 15)
    @ApiModelProperty(value = "车辆状态")
    private java.lang.String status;
	/**车辆任务状态*/
	@Excel(name = "车辆任务状态", width = 15)
    @ApiModelProperty(value = "车辆任务状态")
    private java.lang.String taskStatus;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private java.lang.String remarks;
	/**所属部门*/
	@Excel(name = "所属部门", width = 15)
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;


    /**九识车辆id*/
    @Excel(name = "九识车辆id", width = 15)
    @ApiModelProperty(value = "九识车辆id")
    private java.lang.Integer jsId;
    /**九识车辆名*/
    @Excel(name = "九识车辆名", width = 15)
    @ApiModelProperty(value = "九识车辆名")
    private java.lang.String jsName;
//    /**九识站点id*/
//    @Excel(name = "九识站点id", width = 15)
//    @ApiModelProperty(value = "九识站点id")
//    private java.lang.String js_stationId;
//    /**九识站点名*/
//    @Excel(name = "九识站点名", width = 15)
//    @ApiModelProperty(value = "九识站点名")
//    private java.lang.String js_stationName;
//    /**车辆状态*/
//    @Excel(name = "九识车辆状态", width = 15)
//    @ApiModelProperty(value = "车辆状态")
//    private java.lang.String js_businessStatus;
//    /**车辆状态*/
//    @Excel(name = "九识车辆状态名", width = 15)
//    @ApiModelProperty(value = "九识车辆状态名")
//    private java.lang.String js_businessStatusName;
}
