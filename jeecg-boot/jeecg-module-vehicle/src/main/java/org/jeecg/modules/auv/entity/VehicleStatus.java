package org.jeecg.modules.auv.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description: 巡逻车辆
 * @Author: jeecg-boot
 * @Date:   2024-08-26
 * @Version: V1.0
 */
@Data
@TableName("auv_vehicle")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="auv_vehicle对象", description="巡逻车辆")
public class VehicleStatus implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键id")
    private Integer id;
	/**车辆编码*/
	@Excel(name = "车辆编码", width = 15)
    @ApiModelProperty(value = "车辆编码")
    private String num;
	/**车辆名称*/
	@Excel(name = "车辆名称", width = 15)
    @ApiModelProperty(value = "车辆名称")
    private String name;
	/**车辆牌照*/
	@Excel(name = "车辆牌照", width = 15)
    @ApiModelProperty(value = "车辆牌照")
    private String licence;
	/**巡逻站id*/
	@Excel(name = "巡逻站id", width = 15)
    @ApiModelProperty(value = "巡逻站id")
    private Integer stationId;
	/**巡逻站名称*/
	@Excel(name = "巡逻站名称", width = 15)
    @ApiModelProperty(value = "巡逻站名称")
    private String stationName;
	/**车辆状态*/
	@Excel(name = "车辆状态", width = 15)
    @ApiModelProperty(value = "车辆状态")
    private String status;
	/**车辆任务状态*/
	@Excel(name = "车辆任务状态", width = 15)
    @ApiModelProperty(value = "车辆任务状态")
    private String taskStatus;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private String remarks;
	/**所属部门*/
	@Excel(name = "所属部门", width = 15)
    @ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
}
