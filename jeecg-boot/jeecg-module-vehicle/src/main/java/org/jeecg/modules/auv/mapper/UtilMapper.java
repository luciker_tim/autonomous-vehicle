package org.jeecg.modules.auv.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

public interface UtilMapper {
    /**
     * 根据名称获取并更新序列值
     * @param name 序列名
     * @return 下一个序列值
     */
    @Update("UPDATE sys_sequence_generator SET current_value = current_value + 1 " +
            "WHERE name = #{name} AND current_value > 0")
    int updateSequence(@Param("name") String name);

    @Select("SELECT current_value AS nextValue FROM sys_sequence_generator " +
            "WHERE name = #{name}")
    Long querySequence(@Param("name") String name);

}