package org.jeecg.modules.auv.websocket;


import cn.hutool.core.collection.CollectionUtil;
import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.modules.auv.service.ITaskService;
import org.jeecg.modules.auv.service.VehicleZelostechService;
import org.jeecg.modules.auv.vo.JsVehicleData;
import org.jeecg.modules.auv.websocket.eneity.StatusSocketMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.websocket.Session;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Component
@Slf4j
public class WebSocketTask {

    @Autowired
    private WebSocketServer webSocketServer;
    @Autowired
    private ITaskService taskService;
    @Autowired
    private VehicleZelostechService vehicleZelostechService;

    /**
     * 通过WebSocket每隔3秒向客户端发送消息
     */
    @Scheduled(cron = "0/3 * * * * ?")
    public void sendMessageToClient() {
        if (CollectionUtil.isEmpty(WebSocketServer.vehicleSessionMap)){
            return;
        }
        Set<Map.Entry<String, String>> entries = WebSocketServer.vehicleSessionMap.entrySet();
        for (Map.Entry<String, String> entry : entries) {
            String userId = entry.getKey();
            String vehicleName = WebSocketServer.vehicleSessionMap.get(userId);
            JsVehicleData vehicleData = vehicleZelostechService.getByVehicleName(vehicleName);
            if (vehicleData != null)
            {
                Session session = WebSocketServer.sessionMap.get(userId);
                String carPosition = "["+vehicleData.getLon() + "," + vehicleData.getLat() + "]";
                session.getAsyncRemote().sendText(carPosition);
            }else{
                log.info(vehicleName + ": 获取车辆数据失败");
            }
        }
    }
}