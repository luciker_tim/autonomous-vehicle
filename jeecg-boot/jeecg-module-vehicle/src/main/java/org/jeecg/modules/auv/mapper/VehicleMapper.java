package org.jeecg.modules.auv.mapper;

import org.jeecg.modules.auv.entity.Vehicle;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 巡逻车辆
 * @Author: jeecg-boot
 * @Date:   2024-08-26
 * @Version: V1.0
 */
public interface VehicleMapper extends BaseMapper<Vehicle> {

}
