package org.jeecg.modules.auv.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.jeecg.modules.auv.config.JiushiConfig;
import org.jeecg.modules.auv.entity.VehiclePoint;
import org.jeecg.modules.auv.service.VehicleZelostechService;
import org.jeecg.modules.auv.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * 九识信息查询接口
 * @author haoRt
 * @date 2024/8/27 20:07
 */
@Slf4j
@Service
public class VehicleZelostechServiceImpl implements VehicleZelostechService {

    private static final String VEHICLE_API_URL = "/vehicle";
    private static final String VEHICLES_API_URL = "/vehicles";
    private static final String STOPS_API_URL = "/stops";
    private static final String STATIONS_API_URL = "/stations";

    @Autowired
    private JiushiConfig jiushiConfig;

    @Override
    public JsVehicleData getByVehicleName(String js_vehicleName) {
        RestTemplate restTemplate = new RestTemplate();
        // 设置headers
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", jiushiConfig.getJiushiToken()); // 示例：添加认证令牌

        // 创建HttpEntity对象
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        // 创建参数map
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl( JiushiConfig.JIUSHI_OPENAPI_URL + VEHICLE_API_URL )
                .queryParam("vehicleName", js_vehicleName);


        // 发起GET请求并返回JSON字符串
        ResponseEntity<Result<JsVehicleData>> response = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<Result<JsVehicleData>>() {}
        );

        JsVehicleData jsvd = response.getBody().getData();
        if (jsvd != null ){
            jsvd.setLatAndLon( jsvd.getLat() + "," + jsvd.getLon() );
            jsvd.setGcj02LatAndLon( jsvd.getGcj02Lat() + "," + jsvd.getGcj02Lon() );
        }

        return response.getBody().getData();
    }

    @Override
    public JsListPageData<JsVehicleData> getVehicleList(String pageNumber, String pageSize, String stationId) {
        RestTemplate restTemplate = new RestTemplate();

        // 设置headers
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", jiushiConfig.getJiushiToken()); // 示例：添加认证令牌
        // 创建HttpEntity对象
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        // 创建参数map
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl( JiushiConfig.JIUSHI_OPENAPI_URL + VEHICLES_API_URL);
        if (pageNumber != null) {
            builder.queryParam("pageNumber", pageNumber);
        }
        if (pageSize != null) {
            builder.queryParam("pageSize", pageSize);
        }
        if (stationId != null) {
            builder.queryParam("stationId", stationId);
        }

        // 发起GET请求并返回Result对象
        ResponseEntity<Result<JsListPageData<JsVehicleData>>> response = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<Result<JsListPageData<JsVehicleData>>>() {} // 指定泛型类型
        );
        return response.getBody().getData();

    }



    @Override
    public JsListPageData<JsStopData> getStopList(String pageNumber, String pageSize, String stationId) {
        RestTemplate restTemplate = new RestTemplate();

        // 设置headers
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", jiushiConfig.getJiushiToken()); // 示例：添加认证令牌
        // 创建HttpEntity对象
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        // 创建参数map
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl( JiushiConfig.JIUSHI_OPENAPI_URL + STOPS_API_URL);
        if (pageNumber != null) {
            builder.queryParam("pageNumber", pageNumber);
        }
        if (pageSize != null) {
            builder.queryParam("pageSize", pageSize);
        }
        if (stationId != null) {
            builder.queryParam("stationId", stationId);
        }

        // 发起GET请求并返回Result对象
        ResponseEntity<Result<JsListPageData<JsStopData>>> response = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<Result<JsListPageData<JsStopData>>>() {} // 指定泛型类型
        );

        JsListPageData<JsStopData> js_page = response.getBody().getData();

        if(js_page != null){
            if(js_page.getList() != null){
                for (JsStopData data : js_page.getList()) {
                    data.setLatAndLon(data.getLat() + "," + data.getLon());
                    data.setGcj02LatAndLon(data.getGcj02Lat() + "," + data.getGcj02Lon());
                }
            }
        }

        return js_page;

    }

    @Override
    public JsListPageData<JsStationData> getStationList(String pageNumber, String pageSize) {
        RestTemplate restTemplate = new RestTemplate();

        // 设置headers
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", jiushiConfig.getJiushiToken()); // 示例：添加认证令牌
        // 创建HttpEntity对象
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        // 创建参数map
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(JiushiConfig.JIUSHI_OPENAPI_URL + STATIONS_API_URL);
        if (pageNumber != null) {
            builder.queryParam("pageNumber", pageNumber);
        }
        if (pageSize != null) {
            builder.queryParam("pageSize", pageSize);
        }

        // 发起GET请求并返回Result对象
        ResponseEntity<Result<JsListPageData<JsStationData>>> response = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<Result<JsListPageData<JsStationData>>>(){} // 指定泛型类型
        );

        processStationData(response.getBody());
        return response.getBody().getData();
    }


    @Override
    public ArrayList<VehiclePoint> getStopsByVehicleName(String js_vehicleName) {
        RestTemplate restTemplate = new RestTemplate();
        // 设置headers
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", jiushiConfig.getJiushiToken()); // 示例：添加认证令牌

        // 创建HttpEntity对象
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        // 创建参数map
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl( JiushiConfig.JIUSHI_OPENAPI_URL + VEHICLE_API_URL )
                .queryParam("vehicleName", js_vehicleName);

        // 发起GET请求并返回JSON字符串
        ResponseEntity<Result<JsVehicleData>> response = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<Result<JsVehicleData>>() {}
        );

        JsVehicleData jsvd = response.getBody().getData();
        ArrayList<VehiclePoint> vps = this.processStopsData(jsvd);

        return vps;
    }



    private static void processStationData(Result<JsListPageData<JsStationData>> result) {

        JsListPageData<JsStationData> js_page = result.getData();
        if (js_page != null) {
            if (js_page.getList() != null) {
                for (JsStationData data : js_page.getList()) {
                    data.setLatAndLon(data.getLat() + "," + data.getLon());
                    data.setGcj02LatAndLon(data.getGcj02Lat() + "," + data.getGcj02Lon());
                }
            }
        }
    }

    private static ArrayList<VehiclePoint>  processStopsData(JsVehicleData jsVehicleData) {
        List<JsStopData> stops = jsVehicleData.getStops();
        ArrayList<VehiclePoint> vps = new ArrayList();
        if (stops == null){
            return null;
        }
        for (JsStopData stop:stops) {
            VehiclePoint vp = new VehiclePoint();
            vp.setJsVehicleId(jsVehicleData.getId());
            vp.setJsStopId(stop.getId());
            vps.add(vp);
        }
        return vps;
    }





}

