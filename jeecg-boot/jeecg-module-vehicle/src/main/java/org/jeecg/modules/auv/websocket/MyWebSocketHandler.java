package org.jeecg.modules.auv.websocket;

import javax.websocket.server.ServerEndpoint;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;

@ServerEndpoint("/auv/vehicle/test")
public class MyWebSocketHandler {

    @OnOpen
    public void onOpen(Session session) {
        System.out.println("WebSocket connection opened: " + session.getId());
    }

    @OnMessage
    public String onMessage(String message, Session session) {
        System.out.println("Received message: " + message);
        return "Echo: " + message;
    }

    @OnClose
    public void onClose(Session session) {
        System.out.println("WebSocket connection closed: " + session.getId());
    }

    @OnError
    public void onError(Throwable error, Session session) {
        System.out.println("Error occurred: " + error.getMessage());
    }
}