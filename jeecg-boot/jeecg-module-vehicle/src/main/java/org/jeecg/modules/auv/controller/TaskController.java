package org.jeecg.modules.auv.controller;

import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.collection.CollectionUtil;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.auv.entity.Task;
import org.jeecg.modules.auv.entity.TaskPoint;
import org.jeecg.modules.auv.service.ITaskService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.apache.shiro.authz.annotation.RequiresPermissions;

 /**
 * @Description: 巡逻任务
 * @Author: jeecg-boot
 * @Date:   2024-08-26
 * @Version: V1.0
 */
@Api(tags="巡逻任务")
@RestController
@RequestMapping("/auv/task")
@Slf4j
public class TaskController extends JeecgController<Task, ITaskService> {
	@Autowired
	private ITaskService taskService;

	 //@AutoLog(value = "开始巡逻")
	 @ApiOperation(value="开始巡逻", notes="开始巡逻")
	 @PostMapping(value = "/setOut")
	 public Result<String> setOut(@RequestBody Task task) {
		 String s = taskService.setOut(task.getId());
		 return Result.OK(s);
	 }

	 //@AutoLog(value = "取消任务")
	 @ApiOperation(value="取消任务", notes="取消任务")
	 @PostMapping(value = "/cancleSetOut")
	 public Result<String> cancleSetOut(@RequestBody Task task) {
		 return Result.OK(taskService.cancleSetOut(task));
	 }

	/**
	 * 分页列表查询
	 *
	 * @param task
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "巡逻任务-分页列表查询")
	@ApiOperation(value="巡逻任务-分页列表查询", notes="巡逻任务-分页列表查询")
	@GetMapping(value = "/list")
	public Result<IPage<Task>> queryPageList(Task task,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<Task> queryWrapper = QueryGenerator.initQueryWrapper(task, req.getParameterMap());
		Page<Task> page = new Page<Task>(pageNo, pageSize);
		IPage<Task> pageList = taskService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param task
	 * @return
	 */
	@AutoLog(value = "巡逻任务-添加")
	@ApiOperation(value="巡逻任务-添加", notes="巡逻任务-添加")
//	@RequiresPermissions("auv:auv_task:add")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody Task task) {
		taskService.saveTask(task);
//		taskService.save(task);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param task
	 * @return
	 */
	@AutoLog(value = "巡逻任务-编辑")
	@ApiOperation(value="巡逻任务-编辑", notes="巡逻任务-编辑")
//	@RequiresPermissions("auv:auv_task:edit")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody Task task) {
		taskService.updateTask(task);
//		taskService.updateById(task);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "巡逻任务-通过id删除")
	@ApiOperation(value="巡逻任务-通过id删除", notes="巡逻任务-通过id删除")
//	@RequiresPermissions("auv:auv_task:delete")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		taskService.removeTask(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "巡逻任务-批量删除")
	@ApiOperation(value="巡逻任务-批量删除", notes="巡逻任务-批量删除")
//	@RequiresPermissions("auv:auv_task:deleteBatch")
//	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.taskService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "巡逻任务-通过id查询")
	@ApiOperation(value="巡逻任务-通过id查询", notes="巡逻任务-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<Task> queryById(@RequestParam(name="id",required=true) String id) {
		Task task = taskService.getById(id);
		if(task==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(task);
	}

	 /**
	  * 通过id查询任务的停靠点
	  *
	  * @param id
	  * @return
	  */
	 //@AutoLog(value = "巡逻任务-通过id查询")
	 @ApiOperation(value="巡逻任务-通过id查询停靠点List", notes="巡逻任务-通过id查询停靠点List")
	 @GetMapping(value = "/queryTaskPointByTaskId")
	 public Result<List<TaskPoint>> queryTaskPointByTaskId(@RequestParam(name="id",required=true) String id) {
		 List<TaskPoint> taskPointList = taskService.queryTaskPointByTaskId(id);
		 if(CollectionUtil.isEmpty(taskPointList)) {
			 return Result.error("未找到对应数据");
		 }
		 return Result.OK(taskPointList);
	 }

	 /**
	  * 通过id查询任务的停靠点
	  *
	  * @param id
	  * @return
	  */
	 //@AutoLog(value = "巡逻任务-通过id查询")
	 @ApiOperation(value="巡逻任务-通过任务id查询停靠点路线", notes="巡逻任务-通过任务id查询停靠点路线")
	 @GetMapping(value = "/queryRouteByTaskId")
	 public Result<List<TaskPoint>> queryRouteByTaskId(@RequestParam(name="id",required=true) String id) {
		 String route = taskService.queryRouteByTaskId(id);
		 return Result.OK(route);
	 }

    /**
    * 导出excel
    *
    * @param request
    * @param task
    */
    //@RequiresPermissions("auv:auv_task:exportXls")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, Task task) {
        return super.exportXls(request, task, Task.class, "巡逻任务");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    //@RequiresPermissions("auv:auv_task:importExcel")
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, Task.class);
    }

}
