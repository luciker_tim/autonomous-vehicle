package org.jeecg.modules.auv.util;

import lombok.extern.slf4j.Slf4j;
import org.jeecg.modules.auv.mapper.UtilMapper;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Slf4j
@Component
public class MysqlUtil {
    @Resource
    UtilMapper sequenceMapper;

    @Transactional
    public long getNextSequenceValue(String name) {
        // 更新序列值
        sequenceMapper.updateSequence(name);

        // 查询更新后的序列值
        Long nextValue = sequenceMapper.querySequence(name);

        if (nextValue == null || nextValue <= 0) {
            throw new IllegalStateException("Sequence value for '" + name + "' is exhausted.");
        }

        return nextValue;
    }

}
