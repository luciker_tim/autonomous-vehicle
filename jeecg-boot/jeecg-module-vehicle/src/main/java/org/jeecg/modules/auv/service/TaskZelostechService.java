package org.jeecg.modules.auv.service;

import org.jeecg.modules.auv.vo.SoundRequest;

/**
 * 九识远程控制接口
 * @author haoRt
 * @date 2024/8/26 20:08
 */
public interface TaskZelostechService {

    boolean playSound(SoundRequest soundRequest);

}
