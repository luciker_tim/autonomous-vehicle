package org.jeecg.modules.auv.service;

import org.jeecg.modules.auv.entity.Vehicle;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 巡逻车辆
 * @Author: jeecg-boot
 * @Date:   2024-08-26
 * @Version: V1.0
 */
public interface IVehicleService extends IService<Vehicle> {
}
