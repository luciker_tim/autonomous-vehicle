package org.jeecg.modules.auv.service;

import org.jeecg.modules.auv.entity.Task;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.auv.entity.TaskPoint;

import java.util.List;

/**
 * @Description: 巡逻任务
 * @Author: jeecg-boot
 * @Date:   2024-08-26
 * @Version: V1.0
 */
public interface ITaskService extends IService<Task> {

    String setOut(String taskId);

    String cancleSetOut(Task task);

    void saveTask(Task task);

    void removeTask(String id);

    void updateTask(Task task);

    List<TaskPoint> queryTaskPointByTaskId(String id);

    String queryRouteByTaskId(String id);
}
