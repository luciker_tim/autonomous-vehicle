package org.jeecg.modules.auv.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 任务巡逻点
 * @Author: jeecg-boot
 * @Date:   2024-08-26
 * @Version: V1.0
 */
@Data
@TableName("auv_task_point")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="auv_task_point对象", description="任务巡逻点")
public class TaskPoint implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键id")
    private java.lang.String id;
	/**巡逻点id*/
	@Excel(name = "巡逻点id", width = 15)
    @ApiModelProperty(value = "巡逻点id")
    private java.lang.String pointId;
	/**巡逻点编号*/
	@Excel(name = "巡逻点编号", width = 15)
    @ApiModelProperty(value = "巡逻点编号")
    private java.lang.String pointNum;
	/**巡逻点名称*/
	@Excel(name = "巡逻点名称", width = 15)
    @ApiModelProperty(value = "巡逻点名称")
    private java.lang.String pointName;
	/**停靠时长*/
	@Excel(name = "停靠时长", width = 15)
    @ApiModelProperty(value = "停靠时长")
    private java.lang.String duration;
	/**巡逻点顺序*/
	@Excel(name = "巡逻点顺序", width = 15)
    @ApiModelProperty(value = "巡逻点顺序")
    private java.lang.Integer orderNum;
	/**巡逻任务id*/
	@Excel(name = "巡逻任务id", width = 15)
    @ApiModelProperty(value = "巡逻任务id")
    private java.lang.String taskId;
}
