package org.jeecg.modules.auv.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.auv.entity.TaskPoint;
import org.jeecg.modules.auv.service.ITaskPointService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.apache.shiro.authz.annotation.RequiresPermissions;

 /**
 * @Description: 任务巡逻点
 * @Author: jeecg-boot
 * @Date:   2024-08-26
 * @Version: V1.0
 */
@Api(tags="任务巡逻点")
@RestController
@RequestMapping("/auv/taskPoint")
@Slf4j
public class TaskPointController extends JeecgController<TaskPoint, ITaskPointService> {
	@Autowired
	private ITaskPointService taskPointService;
	
	/**
	 * 分页列表查询
	 *
	 * @param taskPoint
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "任务巡逻点-分页列表查询")
	@ApiOperation(value="任务巡逻点-分页列表查询", notes="任务巡逻点-分页列表查询")
	@GetMapping(value = "/list")
	public Result<IPage<TaskPoint>> queryPageList(TaskPoint taskPoint,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<TaskPoint> queryWrapper = QueryGenerator.initQueryWrapper(taskPoint, req.getParameterMap());
		Page<TaskPoint> page = new Page<TaskPoint>(pageNo, pageSize);
		IPage<TaskPoint> pageList = taskPointService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param taskPoint
	 * @return
	 */
	@AutoLog(value = "任务巡逻点-添加")
	@ApiOperation(value="任务巡逻点-添加", notes="任务巡逻点-添加")
	@RequiresPermissions("auv:auv_task_point:add")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody TaskPoint taskPoint) {
		taskPointService.save(taskPoint);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param taskPoint
	 * @return
	 */
	@AutoLog(value = "任务巡逻点-编辑")
	@ApiOperation(value="任务巡逻点-编辑", notes="任务巡逻点-编辑")
	@RequiresPermissions("auv:auv_task_point:edit")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody TaskPoint taskPoint) {
		taskPointService.updateById(taskPoint);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "任务巡逻点-通过id删除")
	@ApiOperation(value="任务巡逻点-通过id删除", notes="任务巡逻点-通过id删除")
	@RequiresPermissions("auv:auv_task_point:delete")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		taskPointService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "任务巡逻点-批量删除")
	@ApiOperation(value="任务巡逻点-批量删除", notes="任务巡逻点-批量删除")
	@RequiresPermissions("auv:auv_task_point:deleteBatch")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.taskPointService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "任务巡逻点-通过id查询")
	@ApiOperation(value="任务巡逻点-通过id查询", notes="任务巡逻点-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<TaskPoint> queryById(@RequestParam(name="id",required=true) String id) {
		TaskPoint taskPoint = taskPointService.getById(id);
		if(taskPoint==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(taskPoint);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param taskPoint
    */
    @RequiresPermissions("auv:auv_task_point:exportXls")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, TaskPoint taskPoint) {
        return super.exportXls(request, taskPoint, TaskPoint.class, "任务巡逻点");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequiresPermissions("auv:auv_task_point:importExcel")
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, TaskPoint.class);
    }

}
