package org.jeecg.modules.auv.service.impl;

import org.jeecg.modules.auv.entity.TaskPoint;
import org.jeecg.modules.auv.mapper.TaskPointMapper;
import org.jeecg.modules.auv.service.ITaskPointService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 任务巡逻点
 * @Author: jeecg-boot
 * @Date:   2024-08-26
 * @Version: V1.0
 */
@Service
public class TaskPointServiceImpl extends ServiceImpl<TaskPointMapper, TaskPoint> implements ITaskPointService {

}
