package org.jeecg.modules.auv.mapper;

import org.apache.ibatis.annotations.Update;
import org.jeecg.modules.auv.entity.Point;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 巡逻点
 * @Author: jeecg-boot
 * @Date:   2024-08-26
 * @Version: V1.0
 */

public interface PointMapper extends BaseMapper<Point> {


    @Update("update AUV_POINT SET is_active = #{isActive} WHERE id = #{id}")
    public boolean changeActive(Point point);

    @Update("update AUV_POINT SET type = #{type} WHERE id = #{id}")
    public boolean changeType(Point point);
}
