package org.jeecg.modules.auv.quartz.service;

import java.util.List;

import org.jeecg.modules.auv.quartz.entity.AuvQuartzJob;
import org.quartz.SchedulerException;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 定时任务在线管理
 * @Author: jeecg-boot
 * @Date: 2019-04-28
 * @Version: V1.1
 */
public interface IAuvQuartzJobService extends IService<AuvQuartzJob> {

    /**
     * 通过类名寻找定时任务
     * @param jobClassName 类名
     * @return List<QuartzJob>
     */
	List<AuvQuartzJob> findByJobClassName(String jobClassName);

    /**
     * 保存定时任务
     * @param quartzJob
     * @return boolean
     */
	boolean saveAndScheduleJob(AuvQuartzJob quartzJob);

    /**
     * 编辑定时任务
     * @param quartzJob
     * @return boolean
     * @throws SchedulerException
     */
	boolean editAndScheduleJob(AuvQuartzJob quartzJob) throws SchedulerException;

    /**
     * 删除定时任务
     * @param quartzJob
     * @return boolean
     */
	boolean deleteAndStopJob(AuvQuartzJob quartzJob);

    /**
     * 恢复定时任务
     * @param quartzJob
     * @return
     */
	boolean resumeJob(AuvQuartzJob quartzJob);

	/**
	 * 执行定时任务
	 * @param quartzJob
     * @throws Exception
	 */
	void execute(AuvQuartzJob quartzJob) throws Exception;

	/**
	 * 暂停任务
	 * @param quartzJob
	 * @throws SchedulerException
	 */
	void pause(AuvQuartzJob quartzJob);
}
