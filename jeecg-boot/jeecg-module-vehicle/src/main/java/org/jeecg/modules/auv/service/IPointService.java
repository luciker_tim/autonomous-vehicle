package org.jeecg.modules.auv.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.modules.auv.entity.Point;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 巡逻点
 * @Author: jeecg-boot
 * @Date:   2024-08-26
 * @Version: V1.0
 */
public interface IPointService extends IService<Point> {
    public boolean changeActive(Point point);
    public boolean changeType(Point point);

    public IPage<Point> pageList (Page<Point> page, Wrapper<Point> queryWrapper) ;
}
