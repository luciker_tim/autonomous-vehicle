package org.jeecg.modules.auv.service.impl;

import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import org.jeecg.modules.auv.config.JiushiConfig;
import org.jeecg.modules.auv.entity.Point;
import org.jeecg.modules.auv.entity.Task;
import org.jeecg.modules.auv.entity.TaskPoint;
import org.jeecg.modules.auv.entity.Vehicle;
import org.jeecg.modules.auv.mapper.TaskMapper;
import org.jeecg.modules.auv.quartz.entity.AuvQuartzJob;
import org.jeecg.modules.auv.quartz.service.IAuvQuartzJobService;
import org.jeecg.modules.auv.service.IPointService;
import org.jeecg.modules.auv.service.ITaskPointService;
import org.jeecg.modules.auv.service.ITaskService;
import org.jeecg.modules.auv.service.IVehicleService;
import org.jeecg.modules.auv.vo.JsVehicleData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @Description: 巡逻任务
 * @Author: jeecg-boot
 * @Date:   2024-08-26
 * @Version: V1.0
 */
@Service
public class TaskServiceImpl extends ServiceImpl<TaskMapper, Task> implements ITaskService {

    @Autowired
    private ITaskPointService taskPointService;
    @Autowired
    private IPointService pointService;
    @Autowired
    private IVehicleService vehicleService;
    @Autowired
    private IAuvQuartzJobService quartzJobService;

    @Autowired
    private JiushiConfig jiushiConfig;

    @Override
    public String setOut(String taskId) {
        Task byId = getById(taskId);
        LambdaQueryWrapper<TaskPoint> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(TaskPoint::getTaskId, byId.getId()).orderByAsc(TaskPoint::getOrderNum);
        List<TaskPoint> taskPointList = taskPointService.list(wrapper);
//        Collections.sort(taskPointList, new Comparator<TaskPoint>() {
//            @Override
//            public int compare(TaskPoint o1, TaskPoint o2) {
//                return o1.getOrderNum() - o2.getOrderNum(); // 升序排序
//            }
//        });
        int[] toStopIds = new int[taskPointList.size()];
        for (int i = 0; i < toStopIds.length; i++) {
            Point point = pointService.getById(taskPointList.get(i).getPointId());
            toStopIds[i] = point.getJsId();
        }

        JSONObject obj = JSONUtil.createObj();
        Vehicle vehicle = vehicleService.getById(byId.getVehicleId());
        obj.set("vehicleName", vehicle.getJsName());
        obj.set("toStopIds", toStopIds);
        System.out.println(obj);

        String res = HttpRequest.post(JiushiConfig.JIUSHI_OPENAPI_URL + "/vehicle/add_dispatch")
                .header("token", jiushiConfig.getJiushiToken())
                .body(obj.toString(),"application/json")
                .execute().body();

        return res;
    }

    @Override
    public String cancleSetOut(Task task) {
        Vehicle vehicle = vehicleService.getById(task.getVehicleId());
        JSONObject obj = JSONUtil.createObj();
        obj.append("vehicleName", vehicle.getJsName());

        String res = HttpRequest.post(JiushiConfig.JIUSHI_OPENAPI_URL + "/vehicle/cancel_dispatch ")
                .header("token", jiushiConfig.getJiushiToken())
                .body(obj.toString(),"application/json")
                .execute().body();

        return res;
    }

    @Override
    public void saveTask(Task task) {
        // 保存任务和停靠点
        String id = IdWorker.getIdStr();
        task.setId(id);
        save(task);
        taskPointService.saveBatch(task.getTaskPointList());

        // 生成cron表达式
        String cron = generateCronExpression(task.getStartTime());

        // 创建和开启调度任务
        AuvQuartzJob job = new AuvQuartzJob();
        job.setId(id);
        job.setJobClassName("org.jeecg.modules.auv.quartz.job.SetOutJob");
        job.setCronExpression(cron);
        job.setParameter(task.getId());

        quartzJobService.saveAndScheduleJob(job);
    }

    @Override
    public void removeTask(String id) {
        removeById(id);
        AuvQuartzJob job = new AuvQuartzJob();
        job.setId(id);
        quartzJobService.deleteAndStopJob(job);
    }

    @Override
    public void updateTask(Task task) {
        removeTask(task.getId());
        saveTask(task);
    }

    @Override
    public List<TaskPoint> queryTaskPointByTaskId(String id) {
        LambdaQueryWrapper<TaskPoint> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(TaskPoint::getTaskId, id);
        return taskPointService.list(wrapper);
    }

    @Override
    public String queryRouteByTaskId(String id) {
        LambdaQueryWrapper<TaskPoint> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(TaskPoint::getTaskId, id).orderByAsc(TaskPoint::getOrderNum);
        List<TaskPoint> list = taskPointService.list(wrapper);
        StringBuilder builder = new StringBuilder(0);
        builder.append("[");
        for (int i = 0; i < list.size(); i++) {
            TaskPoint taskPoint = list.get(i);
            Point point = pointService.getById(taskPoint.getPointId());
            String position = "[" + point.getLongitude() + "," + point.getLatitude() + "]";
            if (i != list.size() - 1)
            {
                builder.append(position).append(",");
            }
        }
        builder.append("]");
        return builder.toString();
    }

    public String generateCronExpression(String time){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = sdf.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);

        String cornExpression = String.format("%d %d %d %d %d ? %d",
                second, minute, hour, day, month, year);
        return cornExpression;
    }
}
