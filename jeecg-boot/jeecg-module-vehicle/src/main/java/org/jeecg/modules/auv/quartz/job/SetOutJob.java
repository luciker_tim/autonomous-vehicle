package org.jeecg.modules.auv.quartz.job;

import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.auv.service.ITaskService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;


@Slf4j
public class SetOutJob implements Job {

    @Autowired
    private ITaskService taskService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        String taskId = jobExecutionContext.getMergedJobDataMap().getString("parameter");
        String res = taskService.setOut(taskId);
        log.info(res);
    }
}
