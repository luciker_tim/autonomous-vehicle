package org.jeecg.modules.auv.vo;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
public class Result<T> {
    private boolean success;
    @JsonProperty("errorCode")
    private String errorCode;
    private String message;
    @JsonProperty("data")
    @EqualsAndHashCode.Exclude
    private T data;

}
