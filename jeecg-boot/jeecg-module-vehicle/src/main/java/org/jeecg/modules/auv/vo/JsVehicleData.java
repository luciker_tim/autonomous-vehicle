package org.jeecg.modules.auv.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class JsVehicleData implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String name;
    private String number;
    private int stationId;
    private String stationName;
    private List<Integer> gridNos;
    private List<JsStopData> stops;
    private String lon;
    private String lat;
    private String gcj02Lon;
    private String gcj02Lat;
    private boolean online;
    private boolean batteryCharging;
    private double batteryPower;
    private String businessStatus;
    private String businessStatusName;
    private Integer dispatchId;
    private Integer currentGoalIndex;
    private Object goals;

    private String latAndLon;
    private String gcj02LatAndLon;
}