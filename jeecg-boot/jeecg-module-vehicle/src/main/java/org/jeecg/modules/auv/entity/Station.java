package org.jeecg.modules.auv.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: auv_station
 * @Author: jeecg-boot
 * @Date:   2024-08-26
 * @Version: V1.0
 */
@Data
@TableName("auv_station")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="auv_station对象", description="auv_station")
public class Station implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键id")
    private java.lang.String id;
	/**巡逻站编码*/
	@Excel(name = "巡逻站编码", width = 15)
    @ApiModelProperty(value = "巡逻站编码")
    private java.lang.String num;
	/**巡逻站名称*/
	@Excel(name = "巡逻站名称", width = 15)
    @ApiModelProperty(value = "巡逻站名称")
    private java.lang.String name;
	/**巡逻站经度*/
	@Excel(name = "巡逻站经度", width = 15)
    @ApiModelProperty(value = "巡逻站经度")
    private java.lang.String longitude;
	/**巡逻站纬度*/
	@Excel(name = "巡逻站纬度", width = 15)
    @ApiModelProperty(value = "巡逻站纬度")
    private java.lang.String latitude;
	/**巡逻站（国际）经度*/
	@Excel(name = "巡逻站（国际）经度", width = 15)
    @ApiModelProperty(value = "巡逻站（国际）经度")
    private java.lang.String interLongitude;
	/**巡逻站（国际）纬度*/
	@Excel(name = "巡逻站（国际）纬度", width = 15)
    @ApiModelProperty(value = "巡逻站（国际）纬度")
    private java.lang.String interLatitude;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private java.lang.String remarks;
	/**所属部门*/
	@Excel(name = "所属部门", width = 15)
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
    /**九识id*/
    @Excel(name = "九识巡逻站id", width = 15)
    @ApiModelProperty(value = "九识巡逻站id")
    private java.lang.Integer jsId;
}
