package org.jeecg.modules.auv.service.impl;

import org.jeecg.modules.auv.entity.Station;
import org.jeecg.modules.auv.mapper.StationMapper;
import org.jeecg.modules.auv.service.IStationService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: auv_station
 * @Author: jeecg-boot
 * @Date:   2024-08-26
 * @Version: V1.0
 */
@Service
public class StationServiceImpl extends ServiceImpl<StationMapper, Station> implements IStationService {

}
